from prefect import flow
from prefect.task_runners import SequentialTaskRunner
from tasks import extract, transform, load, clean
from tempfile import mktemp
from utils import get_google_worksheet


@flow(name="ETL para os censos do INEP",
      task_runner=SequentialTaskRunner())
def etl_inep_censo(url=None, query="", tblname=None, sep=",", encoding="utf-8"):
    if url is None:
        raise ValueError("URL not defined")
    if tblname is None:
        raise ValueError("Table name not defined")

    # Create tmp filename for saving
    fname = f"{mktemp()}.csv"
    
    fpath = extract.submit(url, fname)

    chunks = transform.submit(fpath, query, sep=sep, encoding=encoding)

    load.submit(chunks, tblname)

    clean.submit(fpath)

@flow
def update_warehouse_resources():
    metadata = get_google_worksheet(spreadsheet_key='1WOeO-eIDV-CBOyO5GWT3zFa_fYIYhzDu6XpNY2t7VsQ',
                                    service_file="obsd-373119-41cbd423ec3c.json")
    resources = metadata.worksheet_by_title("Recursos").get_as_df()
    for index, resource in resources.iterrows():
        etl_inep_censo(url=resource["url_datalake"],
                       query=resource["filter_query"],
                       tblname=resource["tblname_warehouse"],
                       sep=resource["csv_sep"],
                       encoding=resource["csv_encoding"])


if __name__ == "__main__":
    update_warehouse_resources()
