"""
Descricao do module
"""
from prefect import task, get_run_logger
from pathlib import Path
import pandas as pd
from datetime import datetime
from utils import download_file
from sqlalchemy import create_engine


@task(name="Extrai URL para um arquivo")
def extract(url, fname):
    fpath = download_file(url, fname)
    return Path(fpath)


@task(name="Filtra tabela baseado em uma query")
def transform(fpath, qry, sep=",", encoding="utf-8", chunk_size=10000):
    logger = get_run_logger()
    logger.info(f"Transforming {fpath}")

    iter_csv = pd.read_csv(
        fpath,
        iterator=True,
        sep=sep,
        encoding='iso-8859-1',
        chunksize=chunk_size)

    start_time = datetime.now()
    df = [chunk.query(qry) for chunk in iter_csv]
    end_time = datetime.now()

    logger.info(f"Time for filtering {fpath}: {end_time - start_time}.")
    return df


@task(name="Carrega lista de Dataframes para o PostgreSQL")
def load(chunks, tblname):
    # Credentials for database
    dbms = "postgresql"
    dbname = "atlas_capital_humano"
    dbuser = "postgres"
    dbhost = "postgres"
    dbport = "5432"
    dbpassword = "iplanfordockerpostgres"

    # Instantiate sqlachemy.create_engine object
    engine_sql = f"{dbms}://{dbuser}:{dbpassword}@{dbhost}:{dbport}/{dbname}"
    engine = create_engine(engine_sql)

    chunks[0].to_sql(tblname, engine, if_exists="replace")

    for chunk in chunks:
        chunk.to_sql(tblname, engine, if_exists="append")


@task
def clean(fpath):
    fpath.unlink()
