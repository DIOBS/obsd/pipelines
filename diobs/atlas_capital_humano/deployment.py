from flows import etl_inep_censo
from prefect.deployments import Deployment
from prefect.filesystems import RemoteFileSystem
from prefect.infrastructure import Process
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

#################################
# Create database if not exists #
#################################
# Connect to PostgreSQL DBMS
conn = psycopg2.connect(database="postgres", host="postgres", user="postgres",
                        password="iplanfordockerpostgres", port="5432")
conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
# Obtain a DB Cursor
cursor = conn.cursor()
dbname = "atlas_capital_humano"
# Create a table in PostgreSQL database
cursor.execute(f"select 1 from pg_database where datname='{dbname}'")
if not bool(cursor.rowcount):
    cursor.execute(f"create database {dbname};")
else:
    print(f"Database {dbname} already exists")
conn.close()


minio_block = RemoteFileSystem(
    basepath="s3://prefect-flows/atlas_capital_humano",
    key_type="hash",
    settings=dict(
        use_ssl=False,
        key="prefect",
        secret="dockerprefect",
        client_kwargs=dict(endpoint_url="http://minio:9000")
    ),
)
minio_block.save("minio", overwrite=True)

###########################
# Atlas do Capital Humano #
###########################
deployment_atlas_capital_humano = Deployment.build_from_flow(
    name="ETL para filtrar dados do Censo",
    flow=etl_inep_censo,
    storage=RemoteFileSystem.load('minio'),
    infrastructure=Process(
        name="Hello World flow process",
        env={"ENDPOINT_URL": 'http://minio:9000'}
    ),
    parameters = {"url": None, "query": None, "csv": None}
)
deployment_atlas_capital_humano.apply()
