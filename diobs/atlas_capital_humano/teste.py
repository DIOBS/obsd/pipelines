import pygsheets

def get_google_worksheet(key = None, service_file = None):
    gc = pygsheets.authorize(service_file=service_file)
    return gc.open_by_key(key)

#sh = get_google_worksheet(key = '1WOeO-eIDV-CBOyO5GWT3zFa_fYIYhzDu6XpNY2t7VsQ',
#                          service_file = "obsd-373119-41cbd423ec3c.json")

#print(sh.worksheet_by_title("conteudo").get_as_df())

