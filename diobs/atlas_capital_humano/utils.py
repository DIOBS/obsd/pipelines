import requests
import pathlib
import pygsheets
from prefect import get_run_logger


def get_google_worksheet(spreadsheet_key=None, service_file=None):
    if spreadsheet_key is None:
        raise ValueError("No spreadsheet to download")
    if service_file is None:
        raise ValueError("No service file given")

    logger = get_run_logger()
    logger.info(f"Fetching Google Spreadsheet id={spreadsheet_key}...")
    gc = pygsheets.authorize(service_file=service_file)
    return gc.open_by_key(spreadsheet_key)


def download_file(url=None, filename=None, chunk_size=8192):
    if url is None:
        raise ValueError("No URL to download")
    if filename is None:
        raise ValueError("No output file")

    logger = get_run_logger()
    header = {"user-agent": "Wget/1.16 (linux-gnu)"}

    if pathlib.Path(filename).exists():
        logger.info(f"File {filename} already exists.")
        return filename

    logger.info(f"Attempting to download from {url}...")

    with requests.get(url, stream=True, headers=header) as r:
        r.raise_for_status()
        with open(filename, "wb") as f:
            for chunk in r.iter_content(chunk_size=chunk_size):
                f.write(chunk)

    logger.info(f"URL {url} sucessfully downloaded to {filename}!")
    return filename
