from diobs.atlas_capital_humano.flows import etl_inep_censo

from prefect.deployments import Deployment
from prefect.filesystems import RemoteFileSystem
from prefect.infrastructure import Process

minio_block = RemoteFileSystem(
    basepath="s3://prefect-flows/atlas_capital_humano",
    key_type="hash",
    settings=dict(
        use_ssl=False,
        key="prefect",
        secret="dockerprefect",
        client_kwargs=dict(endpoint_url="http://minio:9000")
    ),
)
minio_block.save("minio", overwrite=True)

###########################
# Atlas do Capital Humano #
###########################
deployment_atlas_capital_humano = Deployment.build_from_flow(
    name="World",
    flow=etl_inep_censo,
    storage=RemoteFileSystem.load('minio'),
    infrastructure=Process(
        name="Hello World flow process",
        env={"ENDPOINT_URL": 'http://minio:9000'}
    )
)
deployment_atlas_capital_humano.apply()
